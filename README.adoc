= Fedora Accessibility (A11y)
:doc-dei: https://docs.fedoraproject.org/en-US/diversity-inclusion/
:license-image: https://licensebuttons.net/l/by/4.0/88x31.png
:license-legal: https://creativecommons.org/licenses/by/4.0/
:license-shield: https://img.shields.io/badge/License-CC%20BY%204.0-blue.svg
:toc:

[link={license-legal}]
image::{license-shield}[License: Creative Commons Attribution 4.0 International (CC BY 4.0)]

_An organizational home to accessibility (a11y) work in Fedora, managed by the link:{doc-dei}[Fedora Diversity, Equity, & Inclusion Team]_.


[[about]]
== About this repository

This repository is used for the following purposes:

* Source content for Fedora Accessibility (a11y) documentation (i.e. `docs.fedoraproject.org/en-US/diversity-inclusion/a11y/`).
* Program management tool and experimental workflow for managing accessibility efforts in the Fedora community.

//TODO What can people do here? What kinds of requests can they make?

See the https://gitlab.com/fedora/dei/a11y/-/issues[GitLab issues] for up-to-date information.


[[participate]]
== How to participate

For now, this repository is in a trial phase.
This may change!
The best thing to use it for is to flag a task, community need, or question that needs the attention of our team.
Open an issue if there is something you want to share or discuss.


[[legal]]
== Legal

[link={license-legal}]
image::{license-image}[License: Creative Commons Attribution 4.0 International (CC BY 4.0)]

All content in this repository is shared under the {license-legal}[Creative Commons Attribution 4.0 International (CC BY 4.0) license].
